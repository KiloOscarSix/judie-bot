# New Judie update to Version 2.0 is here!
### Update requires updating the database. Please do not delete your account before updating, as this will result in an irreversible loss of data.

Changelog:

## Introducing the brand new Eternum GF GAME
 - Meet your favourite (or less favourite) eternum characters in a brand new edition of the gf game, with new cosmetic improvements compared to the oialt ones.
 - We're starting off with __4 new collections__ including:

### The harem, a classic. (-eharem)
 Members include Alex, Annie, Dalia, Luna, Nancy, Nova & Penny (/7).
 Beware of Thanatos, who is always on the hunt for their heads, or protect yourself with the skilled Calypso, who will make your favourite girls evacuate in time to save them!

### The homies, a group of people you can always trust! (-homies)
 Members include Chang, Chop Chop, Mr. Hernandez, Jerry, Micaela, Noah, Orion & Raul (/8).
 Beware of the vicious Troll, who will go out of its way to smash in their faces, or let Dalia save your homies from it!

### The side girls, a group of horny ladies ready to smash at any time. (-sidegirls)
 Members include: Blue Fox Maiden, Calypso, Eva, Idriel, Maat, Red Fox Maiden & Wenlin (/7).
 Beware of Axel, who sets out to get laid at any given opportunity, or let Orion thwart his plans with a good old fist to the face!

### Finally, the creatures, some friendly (?) pets ready to give you support in difficult times (?). (-creatures)
 Members include: Carolyn, Igor, Kermit, Maurice, Maurice, Maurice & Xenomorph (/7).
 Beware of the nasty Golem, who would like nothing better than to stomp your beloved companions to the ground, or let Pyramid Head punish it by sending it to the depths of hell.

## Nsfw command has been updated to 0.4 contents!
 I would like to sincerely apologize for the missing update to 0.3 contents, and hope you will enjoy it updated to the absolute newest stand now. 

 Added lewds for Nova, Luna, Calypso, Maat, Fox Maidens, and many more

## Finally, I made some minor changes to the code of the OiaLt Gf Game, just for easier maintenance.
 You may also note the additions of the -oprotectors & -ocollections command, who give you better overviews over your oialt collections.
 I consider the OiaLt GF Game as a finished state, and will therefore not touch the game anymore until I feel like making a complete remaster.

Have fun boys and girls!
