## Changelog Judie V2.0

**Uploaded 9. September 2022**

Major update (was about time)

### Changes:

- implemented the new Eternum GF Game

- updated NSFW commands to include lewds from Eternum 0.3 and 0.4.

- Added -oprotections and -ocollections command to the OiaLt module.

- made minor changes in the OiaLt.py file, most notably to be more flexible for future updates.

## Changelog Judie V1.3.3

**Uploaded 2. February 2022**

Another bugfix update ^^

### Changes (Issue fixed):

- Gf commands were usable outside of the bot-spam channels

## Changelog Judie V1.3.2

**Uploaded 24. January 2022**

Mostly focused on bugfixes now, fixed a number of smaller issues:

### Changes (Issues fixed):

- Cooldown not resetting for users issuing the commands gf; register; gf in that order

- The image of Penny in underwear popping up when asking for an Eva lewd

- The error message for Nova not having any lewds yet displaying text for Luna

- The help command not having been updated to include the train conductor in the OiaLt section


## Changelog Version 1.3.1
**Uploaded 19. January 2022**

Fixed a critical bug

### Changes:

- Noticed a failure to update for users that had the priestess as their last collected potential LI. Fixed that issue

- Changed a few extra smol issues that are really just eye candy.


# New Major Update - JudieBOT V1.3.0
**Uploaded 18. January 2022**

*Update requires updating the database. It's only a minor change, you won't be able to register again, so no risk of data loss normally.*

### Changelog:

- Made the gf game and nsfw commands to separate cogs (plugins if you prefer) to make my reading & maintenance of the code more enjoyable and reduce chances of bugs.

- Adding two new characters to the gf game!
welcoming the *Train Conductor* (now a potential LI, replacing the priestess) and *Fake Hiromi* to the game!
Anyone who had the priestess before the 1.3.0 update will get the Conductor in their collection automatically. Fake Hiromi has no effect whatsoever on the game.

- Adding a brand new kind of content with the nsfw plugin, which will allow you to roll a lot of lewd images from the Once in a Lifetime and Eternum game, with applicable filters! 
The -nsfw command will give you access to one random image of the selected ones. Please note that, as there are no lewd renders of Luna and Nova yet, their filter displays a little troll.
Currently supported filters are (case insensitive): OiaLt, Eternum, Judie, Lauren, Carla, Iris, Jasmine, Aiko, Rebecca, Annie, Dalia, Nancy, Penny/Penelope, Alex/Alexandra, Eva, (Luna, Nova).

- The Judie & Lauren good morning and good night images make their comeback with the -gm and -gn commands!

###  Minor changes include:
- A revisit of the deleteacc command, to ask for confirmation before deleting an account's data
- Some code cleaning
- The move of the client token to a separate file to protect Judie from malicious people among you guys.

**Special thanks to Kainan for helping with the funding to host Judie, as well as the beta testing team who, despite being the biggest spammers I know did help a lot in the process.**
